/*
 * Copyright 2022 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.test.jsonata;

import com.api.jsonata4java.expressions.EvaluateException;
import com.api.jsonata4java.expressions.EvaluateRuntimeException;
import com.api.jsonata4java.expressions.Expressions;
import com.api.jsonata4java.expressions.ParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestPredicateExpressions {

	@Test
	public void test() {

		Expressions expr = null;
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonObj = null;
		String json = "{\n"
				+ "  \"FirstName\": \"Fred\",\n"
				+ "  \"Surname\": \"Smith\",\n"
				+ "  \"Age\": 28,\n"
				+ "  \"Address\": {\n"
				+ "    \"Street\": \"Hursley Park\",\n"
				+ "    \"City\": \"Winchester\",\n"
				+ "    \"Postcode\": \"SO21 2JN\"\n"
				+ "  },\n"
				+ "  \"Phone\": [\n"
				+ "    {\n"
				+ "      \"type\": \"home\",\n"
				+ "      \"number\": \"0203 544 1234\"\n"
				+ "    },\n"
				+ "    {\n"
				+ "      \"type\": \"office\",\n"
				+ "      \"number\": \"01962 001234\"\n"
				+ "    },\n"
				+ "    {\n"
				+ "      \"type\": \"office\",\n"
				+ "      \"number\": \"01962 001235\"\n"
				+ "    },\n"
				+ "    {\n"
				+ "      \"type\": \"mobile\",\n"
				+ "      \"number\": \"077 7700 1234\"\n"
				+ "    }\n"
				+ "  ],\n"
				+ "  \"Email\": [\n"
				+ "    {\n"
				+ "      \"type\": \"work\",\n"
				+ "      \"address\": [\"fred.smith@my-work.com\", \"fsmith@my-work.com\"]\n"
				+ "    },\n"
				+ "    {\n"
				+ "      \"type\": \"home\",\n"
				+ "      \"address\": [\"freddy@my-social.com\", \"frederic.smith@very-serious.com\"]\n"
				+ "    }\n"
				+ "  ],\n"
				+ "  \"Other\": {\n"
				+ "    \"Over 18 ?\": true,\n"
				+ "    \"Misc\": null,\n"
				+ "    \"Alternative.Address\": {\n"
				+ "      \"Street\": \"Brick Lane\",\n"
				+ "      \"City\": \"London\",\n"
				+ "      \"Postcode\": \"E1 6RF\"\n"
				+ "    }\n"
				+ "  }\n"
				+ "}";
		String expression = "Phone[type='office'].number";
		try {
			jsonObj = mapper.readTree(json);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try {
			System.out.println("Using json:\n" + mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObj));
			System.out.println("expression=" + expression);
			expr = Expressions.parse(expression);
		} catch (EvaluateRuntimeException ere) {
			System.out.println(ere.getLocalizedMessage());
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (ParseException ex) {
			Logger.getLogger(TestJsonata.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(TestJsonata.class.getName()).log(Level.SEVERE, null, ex);
		}
		try {
			System.out.println("evaluate returns:");
			JsonNode result = expr.evaluate(jsonObj);
			if (result == null) {
				System.out.println("** no match **");
			} else {
				System.out.println("" + mapper.writerWithDefaultPrettyPrinter().writeValueAsString(result));
			}
		} catch (EvaluateException | JsonProcessingException e) {
			System.err.println(e.getLocalizedMessage());
		}
	}
}
