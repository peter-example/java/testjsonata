/*
 * Copyright 2022 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.test.jsonata;

import com.api.jsonata4java.expressions.EvaluateException;
import com.api.jsonata4java.expressions.EvaluateRuntimeException;
import com.api.jsonata4java.expressions.Expressions;
import com.api.jsonata4java.expressions.ParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestGrouping {

	@Test
	public void test() {

		Expressions expr = null;
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonObj = null;

		String json = "{\n"
				+ "  \"Account\" : {\n"
				+ "    \"Account Name\" : \"Firefly\",\n"
				+ "    \"Order\" : [ {\n"
				+ "      \"OrderID\" : \"order103\",\n"
				+ "      \"Product\" : [ {\n"
				+ "        \"Product Name\" : \"Bowler Hat\",\n"
				+ "        \"ProductID\" : 858383,\n"
				+ "        \"SKU\" : \"0406654608\",\n"
				+ "        \"Description\" : {\n"
				+ "          \"Colour\" : \"Purple\",\n"
				+ "          \"Width\" : 300,\n"
				+ "          \"Height\" : 200,\n"
				+ "          \"Depth\" : 210,\n"
				+ "          \"Weight\" : 0.75\n"
				+ "        },\n"
				+ "        \"Price\" : 34.45,\n"
				+ "        \"Quantity\" : 2\n"
				+ "      }, "
				+ "		 {\n"
				+ "        \"Product Name\" : \"Bowler Hat\",\n"
				+ "        \"ProductID\" : 858383,\n"
				+ "        \"SKU\" : \"0406654608\",\n"
				+ "        \"Description\" : {\n"
				+ "          \"Colour\" : \"Purple\",\n"
				+ "          \"Width\" : 300,\n"
				+ "          \"Height\" : 200,\n"
				+ "          \"Depth\" : 210,\n"
				+ "          \"Weight\" : 0.75\n"
				+ "        },\n"
				+ "        \"Price\" : 11.45,\n"
				+ "        \"Quantity\" : 2\n"
				+ "      },{\n"
				+ "        \"Product Name\" : \"Trilby hat\",\n"
				+ "        \"ProductID\" : 858236,\n"
				+ "        \"SKU\" : \"0406634348\",\n"
				+ "        \"Description\" : {\n"
				+ "          \"Colour\" : \"Orange\",\n"
				+ "          \"Width\" : 300,\n"
				+ "          \"Height\" : 200,\n"
				+ "          \"Depth\" : 210,\n"
				+ "          \"Weight\" : 0.6\n"
				+ "        },\n"
				+ "        \"Price\" : 21.67,\n"
				+ "        \"Quantity\" : 1\n"
				+ "      } ]\n"
				+ "    } ]\n"
				+ "  }\n"
				+ "}";
		String expression = "Account.Order.Product{`Product Name`: Price}";
		try {
			jsonObj = mapper.readTree(json);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try {
			System.out.println("Using json:\n" + mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObj));
			System.out.println("expression=" + expression);
			expr = Expressions.parse(expression);
		} catch (EvaluateRuntimeException ere) {
			System.out.println(ere.getLocalizedMessage());
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (ParseException ex) {
			Logger.getLogger(TestJsonata.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(TestJsonata.class.getName()).log(Level.SEVERE, null, ex);
		}
		try {
			System.out.println("evaluate returns:");
			JsonNode result = expr.evaluate(jsonObj);
			if (result == null) {
				System.out.println("** no match **");
			} else {
				System.out.println("" + mapper.writerWithDefaultPrettyPrinter().writeValueAsString(result));
			}
		} catch (EvaluateException | JsonProcessingException e) {
			System.err.println(e.getLocalizedMessage());
		}
	}
}
